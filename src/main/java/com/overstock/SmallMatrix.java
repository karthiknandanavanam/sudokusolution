package com.overstock;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

/**
 * Created by knandanavanam on 11/2/16.
 */
@AutoProperty
public class SmallMatrix {
  final int rowStart;
  final int columnStart;
  final int rowEnd;
  final int columnEnd;

  public SmallMatrix(int rowStart, int columnStart, int rowEnd, int columnEnd) {
    this.rowStart = rowStart;
    this.columnStart = columnStart;
    this.rowEnd = rowEnd;
    this.columnEnd = columnEnd;
  }

  public int getColumnEnd() {
    return columnEnd;
  }

  public int getColumnStart() {
    return columnStart;
  }

  public int getRowEnd() {
    return rowEnd;
  }

  public int getRowStart() {
    return rowStart;
  }

  public boolean isIn(final int row, final int column) {
    return row >= rowStart && row <= rowEnd && column >= columnStart && column <= columnEnd;
  }

  @Override
  public String toString() {
    return Pojomatic.toString(this);
  }

  @Override
  public int hashCode() {
    return Pojomatic.hashCode(this);
  }

  @Override
  public boolean equals(final Object that) {
    return Pojomatic.equals(this, that);
  }
}
