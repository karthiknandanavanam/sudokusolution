package com.overstock;

import java.util.Set;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

/**
 * Created by knandanavanam on 10/28/16.
 */
@AutoProperty
public class Column {
  private int permanentValue;
  private Set<Integer> cannotHave;
  private Set<Integer> canHave;

  public int getPermanentValue() {
    return permanentValue;
  }

  public void setPermanentValue(int permanentValue) {
    this.permanentValue = permanentValue;
  }

  @Override
  public String toString() {
    return Pojomatic.toString(this);
  }

  @Override
  public int hashCode() {
    return Pojomatic.hashCode(this);
  }

  @Override
  public boolean equals(final Object that) {
    return Pojomatic.equals(this, that);
  }
}
