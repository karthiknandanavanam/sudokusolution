package com.overstock;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by knandanavanam on 10/28/16.
 */
public class SudokuSolution {
  private static final Logger LOGGER = Logger.getLogger(SudokuSolution.class.getName());

  public static void main(final String [] args) throws IOException {
    final Matrix matrix = Matrix.readMatrix("/Users/knandanavanam/workspaceBranches/SudokuSolution/src/main/resources/sudokuHard.matrix");

    LOGGER.info(matrix.printMatrix());
    matrix.updateRowAlreadyContains();
    matrix.updateColumnAlreadyContains();
    matrix.updateSmallMatrixAlreadyContains();

    while (matrix.solve()) {
      LOGGER.info("After Solving");
      LOGGER.info(matrix.printMatrix());
    }

    assert matrix != null;
  }
}
