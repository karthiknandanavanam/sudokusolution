package com.overstock;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

/**
 * Created by knandanavanam on 10/28/16.
 */
public class Matrix {
  private List<Integer> SMALL_MATRIX_INDEX = Lists.newArrayList(0, 3, 6);
  private static final int MAX_INDEX = 9;
  private static final Set<Integer> ALL_POSSIBILITIES = Sets.newHashSet(1, 2, 3, 4, 5, 6, 7, 8, 9);

  private final Column [] [] dataNode;
  private Map<Integer, Set<Integer>> rowAlreadyContains;
  private Map<Integer, Set<Integer>> columnAlreadyContains;
  private Map<SmallMatrix, Set<Integer>> smallMatrixAlreadyContains;

  public Matrix() {
    this.dataNode = new Column[MAX_INDEX][MAX_INDEX];
  }

  public void updateRowAlreadyContains() {
    this.rowAlreadyContains = cannotHaveInRowMap();
  }

  public void updateColumnAlreadyContains() {
    this.columnAlreadyContains = cannotHaveInColumnMap();
  }

  public void updateSmallMatrixAlreadyContains() {
    this.smallMatrixAlreadyContains = cannotHaveInSmallMatrixMap();
  }

  private Map<SmallMatrix, Set<Integer>> cannotHaveInSmallMatrixMap() {
    final Map<SmallMatrix, Set<Integer>> cannotHavesInSmallMatrix = Maps.newHashMap();

    for (int rowStart: SMALL_MATRIX_INDEX) {
      for (int columnStart: SMALL_MATRIX_INDEX) {
        final int rowEnd = rowStart + 2;
        final int columnEnd = columnStart + 2;
        final Set<Integer> cannotHaves = cannotHaveInMatrix(rowStart, columnStart, rowEnd, columnEnd);
        final SmallMatrix matrix = new SmallMatrix(rowStart, columnStart, rowEnd, columnEnd);
        cannotHavesInSmallMatrix.put(matrix, cannotHaves);
      }
    }
    return cannotHavesInSmallMatrix;
  }

  private Set<Integer> cannotHaveInMatrix(int rowStart, int columnStart, int rowEnd, int columnEnd) {
    final Set<Integer> cannotHaves = Sets.newHashSet();
    for (int row = rowStart; row <= rowEnd; row = row + 1) {
      for (int column = columnStart; column <= columnEnd; column = column + 1) {
        if (dataNode[row][column].getPermanentValue() != 0) {
          cannotHaves.add(dataNode[row][column].getPermanentValue());
        }
      }
    }
    return cannotHaves;
  }

  public boolean solve() {
    boolean isAnyUpdate = false;

    for (int row = 0; row < MAX_INDEX; row++) {
      for (int column = 0; column < MAX_INDEX; column++) {
        final Set<Integer> cannotContain = cannotContainSet(row, column);
        final Set<Integer> possibilities = Sets.difference(ALL_POSSIBILITIES, cannotContain);
        if (possibilities.size() == 1 && dataNode[row][column].getPermanentValue() == 0) {
          final Integer value = possibilities.iterator().next();
          dataNode[row][column].setPermanentValue(value);
          updateAlreadyContains(row, column, value);
          isAnyUpdate = true;
        }
      }
    }
    return isAnyUpdate;
  }

  private void updateAlreadyContains(int row, int column, Integer value) {
    rowAlreadyContains.get(row).add(value);
    columnAlreadyContains.get(column).add(value);
    final SmallMatrix smallMatrix = findSmallMatrixOfNode(row, column);
    smallMatrixAlreadyContains.get(smallMatrix).add(value);
  }

  private Set<Integer> cannotContainSet(int row, int column) {
    final Set<Integer> rowAlreadyExists = this.rowAlreadyContains.get(row);
    final Set<Integer> columnAlreadyExists = this.columnAlreadyContains.get(column);
    final Set<Integer> smallMatrixAlreadyContains = alreadyContainsSet(row, column);

    final Set<Integer> allSet = Sets.newHashSet();
    allSet.addAll(rowAlreadyExists);
    allSet.addAll(columnAlreadyExists);
    allSet.addAll(smallMatrixAlreadyContains);
    return allSet;
  }

  private Set<Integer> alreadyContainsSet(final int row, final int column) {
    final SmallMatrix matrix = findSmallMatrixOfNode(row, column);
    return this.smallMatrixAlreadyContains.get(matrix);
  }

  private SmallMatrix findSmallMatrixOfNode(int row, int column) {
    return this.smallMatrixAlreadyContains.keySet().stream()
        .filter(record -> record.isIn(row, column))
        .findFirst()
        .get();
  }

  private Map<Integer, Set<Integer>> cannotHaveInColumnMap() {
    Map<Integer, Set<Integer>> columnAlreadyContainsMap = Maps.newHashMap();
    for (int column = 0; column < MAX_INDEX; column++) {
      final Set<Integer> columnAlreadyContains = columnAlreadyContains(column);
      columnAlreadyContainsMap.put(column, columnAlreadyContains);
    }
    return columnAlreadyContainsMap;
  }

  private Map<Integer, Set<Integer>> cannotHaveInRowMap() {
    Map<Integer, Set<Integer>> rowAlreadyContainsMap = Maps.newHashMap();
    for (int row = 0; row < MAX_INDEX; row++) {
      final Set<Integer> rowAlreadyContains = rowAlreadyContains(row);
      rowAlreadyContainsMap.put(row, rowAlreadyContains);
    }
    return rowAlreadyContainsMap;
  }

  private Set<Integer> columnAlreadyContains(int column) {
    final Set<Integer> cannotHaves = Sets.newHashSet();
    for (int row = 0; row < MAX_INDEX; row++) {
      if (dataNode[row][column].getPermanentValue() != 0) {
        cannotHaves.add(dataNode[row][column].getPermanentValue());
      }
    }
    return cannotHaves;
  }

  private Set<Integer> rowAlreadyContains(int row) {
    final Set<Integer> cannotHaves = Sets.newHashSet();
    for (int column = 0; column < MAX_INDEX; column++) {
      if (dataNode[row][column].getPermanentValue() != 0) {
        cannotHaves.add(dataNode[row][column].getPermanentValue());
      }
    }
    return cannotHaves;
  }

  public String printMatrix() {
    final StringBuilder builder = new StringBuilder("\n");
    for (int row = 0; row < MAX_INDEX; row++) {
      for (int column = 0; column < MAX_INDEX; column++) {
        builder.append(dataNode[row][column].getPermanentValue());
        builder.append(" ");
      }
      builder.append("\n");
    }
    return builder.toString();
  }

  public static Matrix readMatrix(final String fileName) throws IOException {
    final Matrix matrix = new Matrix();

    final BufferedReader reader = new BufferedReader(new FileReader(fileName));

    int matrixRow = 0;
    for (String row = reader.readLine(); row != null; row = reader.readLine()) {
      final char [] columns = row.toCharArray();
      assert columns.length == MAX_INDEX;

      int matrixColumn = 0;
      for (int index = 0; index < MAX_INDEX; index++) {
        final int columnValue = Character.getNumericValue(columns[index]);
        final Column column = new Column();
        column.setPermanentValue(columnValue);
        matrix.dataNode[matrixRow][matrixColumn] = column;

        matrixColumn++;
      }

      matrixRow++;
    }

    return matrix;
  }
}
